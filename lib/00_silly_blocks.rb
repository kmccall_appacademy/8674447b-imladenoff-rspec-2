def reverser
  output = []
  yield.split.each do |word|
    output << word.reverse
  end
  output.join " "
end

def adder num=1
  yield + num
end

def repeater reps=1
  reps.times { yield }
end
