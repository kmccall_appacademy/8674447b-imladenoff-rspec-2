def measure reps=1
  start_time = Time.now
  reps.times { yield }
  (Time.now - start_time) / reps
end
